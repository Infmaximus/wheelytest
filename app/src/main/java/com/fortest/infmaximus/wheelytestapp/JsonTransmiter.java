package com.fortest.infmaximus.wheelytestapp;

import android.util.Log;

import org.codehaus.jackson.map.deser.impl.ExternalTypeHandler;
import org.json.JSONObject;

/**
 * Created by InfMaximus on 23.06.2016.
 */
public class JsonTransmiter {

    String TAG = "LOG_JSON";
    String LAT = "lat";
    String LON = "lon";

    public JsonTransmiter(){
    }

    public void sendJsonPosition(double lat, double lon){

        try {
            JSONObject positionJson = new JSONObject();

            positionJson.put(LAT, lat);
            positionJson.put(LON, lon);

            String positionStr = positionJson.toString();
            SimpleWebSocketConnector.getInstance().sendByWebSocket(positionStr);

        }catch (Exception ex){
            Log.e(TAG,ex.toString());
        }
    }


}
