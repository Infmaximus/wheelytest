package com.fortest.infmaximus.wheelytestapp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.provider.SyncStateContract;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, SimpleWebSocketConnector.MapInterface{

    private final int MY_PERMISSIONS_REQUEST_GET_GPS = 1;
    private final String extraUsername = "username";
    private final String extraPassword = "password";
    private final String STARTFOREGROUND_ACTION = "start";
    private final String STOPTFOREGROUND_ACTION = "stop";
    private String keyMap = "AIzaSyBGV-g52pBrz5vLelv_G3uYblJ_eeOU-QI";
    private GoogleMap mMap;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.setRetainInstance(true);
        mapFragment.getMapAsync(this);

        context = this;

        FloatingActionButton FAB = (FloatingActionButton) findViewById(R.id.activity_main_fab);

        FAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent service = new Intent(context, WheelyService.class);
                service.setAction(STOPTFOREGROUND_ACTION);
                startService(service);

                Intent startMapIntent = new Intent(context, AuthorisationActivity.class);
                startMapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startMapIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(startMapIntent);

            }
        });

        String username = "";
        String password = "";

        checkGPS();
        if (getIntent().hasExtra(extraUsername)) {
            username = getIntent().getStringExtra(extraUsername);
        }
        if (getIntent().hasExtra(extraPassword)) {
            password = getIntent().getStringExtra(extraPassword);
        }
        final Intent service = new Intent(this, WheelyService.class);
        service.putExtra(extraUsername, username);
        service.putExtra(extraPassword, password);
        service.setAction(STARTFOREGROUND_ACTION);
        startService(service);

        SimpleWebSocketConnector.getInstance().registerCallBack(this);

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        LatLng randomPosition = new LatLng(55.384017, 37.542654);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(randomPosition));
    }

    private void checkGPS() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_GET_GPS);

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_GET_GPS: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    mMap.setMyLocationEnabled(true);
                }
            }
        }
    }

    @Override
    public void addMapPosition(ArrayList<MapPosition> mps) {
        Log.d("LOG_TAG","get mps maper");
        try {

            boolean firstMap = true;
            mMap.clear();
            for (MapPosition mp:mps){

                LatLng newPoint = new LatLng(mp.getLat(),mp.getLon());
                mMap.addMarker(new MarkerOptions().position(newPoint).title(""+mp.getId()));
                if(firstMap) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(newPoint,
                            11));
                    firstMap = false;
                }
            }

        }catch (Exception ex){

        }
    }

    @Override
    public void onBackPressed() {

        new MaterialDialog.Builder(this)
                .content(R.string.map_activity_sure_exit)
                .positiveText(R.string.utils_exit)
                .negativeText(R.string.utils_cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent service = new Intent(context, WheelyService.class);
                        service.setAction(STOPTFOREGROUND_ACTION);
                        startService(service);

                        Intent startMapIntent = new Intent(context, AuthorisationActivity.class);
                        startMapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startMapIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(startMapIntent);
                    }})
                .show();
    }

}
