package com.fortest.infmaximus.wheelytestapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

public class AuthorisationActivity extends AppCompatActivity implements SimpleWebSocketConnector.AuthorisationInterface{


    private Context context;
    private EditText eUsername;
    private EditText ePassword;
    private Button   button;
    private View     mProgressView;
    private View    mLoginFormView;
    private String username;
    private String password;

    private final String extraUsername = "username";
    private final String extraPassword = "password";

    private Animation shakeanimation;
    private boolean   connectionIsOk = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = this;

        eUsername = (EditText)findViewById(R.id.username);
        ePassword = (EditText)findViewById(R.id.password);
        button    = (Button) findViewById(R.id.sign_in_button);
        mProgressView = findViewById(R.id.login_progress);
        shakeanimation = AnimationUtils.loadAnimation(this,R.anim.shake);

        eUsername.setText("auser");
        //ePassword.setText("apass");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setUsername(eUsername.getText().toString());
                setPassword(ePassword.getText().toString());

                if (isValidString(username) && isValidString(password)) {
                    MyAsyncTask myAsyncTask = new MyAsyncTask(context);
                    myAsyncTask.execute();

                }else {
                    button.startAnimation(shakeanimation);
                    Toast.makeText(context,getString(R.string.authorisation_error_empty),
                            Toast.LENGTH_LONG).show();
                }

            }
        });

        if (SimpleWebSocketConnector.getInstance().isConnection()) {
            Intent startMapIntent = new Intent(context, MapsActivity.class);
            startMapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMapIntent);
        }


    }

    //проверяем логин и пароль на допустимые символы
    private boolean isValidString(String us){
        if(!TextUtils.isEmpty(us)&&us.matches("[a-zA-Z0-9.(@)_а-яА-Я]+")){
            return true;
        }else
            return false;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    //метод обратного вызова результатов подключения
    @Override
    public void authorisationResult(boolean successful,int reason) {
        connectionIsOk = successful;
        if(connectionIsOk)
            new WaiterClose().execute();
        else
        {
            switch (reason){
                case 2:
                    button.startAnimation(shakeanimation);
                    Toast.makeText(context,getString(R.string.authorisation_error_unresolved),
                            Toast.LENGTH_LONG).show();
                    break;
            }
        }

    }

   //Убеждаемся в успешном подключении и стартуем карту
    private class WaiterClose extends AsyncTask<String, Integer, Integer> {
        protected ProgressDialog pd;

        @Override
        protected Integer doInBackground(String... parameter) {
            //проверяем,не произойдет ли разрыв
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            pd.dismiss();
            super.onPostExecute(integer);

            if(connectionIsOk) {
                Intent startMapIntent = new Intent(context, MapsActivity.class);
                startMapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startMapIntent.putExtra(extraUsername,getUsername());
                startMapIntent.putExtra(extraPassword,getPassword());
                startActivity(startMapIntent);
            }else if (SimpleWebSocketConnector.getInstance().isConnection()){
                Intent startMapIntent = new Intent(context, MapsActivity.class);
                startMapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startMapIntent);
            }else {
                button.startAnimation(shakeanimation);
                Toast.makeText(context,getString(R.string.authorisation_error_incorrect),
                        Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onPreExecute() {
            pd = ProgressDialog.show(context, "",getString(R.string.authorisation_try_connect) , true, false);
            super.onPreExecute();

        }
    }

    //Подключаемся к серверу
    private class MyAsyncTask extends AsyncTask<String, Integer, Integer> {
        private Context context;
        protected ProgressDialog pd;
        public MyAsyncTask(Context context) {
            this.context = context;
        }
        @Override
        protected Integer doInBackground(String... parameter) {
            SimpleWebSocketConnector SWSC = SimpleWebSocketConnector.getInstance();
            SWSC.setAuthorisation(getUsername(),getPassword());
            SWSC.disconnectWebSocket();
            SWSC.startWebSocket();
            SWSC.registerCallBackAuthorisation((SimpleWebSocketConnector.AuthorisationInterface) context);
            return null;
        }
    }

    //Нажатие кнопки назад
    @Override
    public void onBackPressed() {

            new MaterialDialog.Builder(this)
                    .content(R.string.authorisation_sure_exit)
                    .positiveText(R.string.utils_exit)
                    .negativeText(R.string.utils_cancel)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            finish();
                        }})
                    .show();

        }
}
