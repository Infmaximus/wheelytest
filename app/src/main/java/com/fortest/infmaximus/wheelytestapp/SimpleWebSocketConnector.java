package com.fortest.infmaximus.wheelytestapp;

import android.util.Log;

import com.koushikdutta.async.AsyncServer;
import com.koushikdutta.async.AsyncSocket;
import com.koushikdutta.async.ByteBufferList;
import com.koushikdutta.async.callback.CompletedCallback;
import com.koushikdutta.async.callback.DataCallback;
import com.koushikdutta.async.callback.WritableCallback;
import com.koushikdutta.async.http.WebSocket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketHandler;
import de.tavendo.autobahn.WebSocketOptions;

/**
 * Created by InfMaximus on 24.06.2016.
 * "ws://mini-mdt.wheely.com?username=a&password=ahappy"
 */
public class SimpleWebSocketConnector {

    private static volatile SimpleWebSocketConnector singleton;
    private String _username;
    private String _password;
    private final String severRequestStr = "ws://mini-mdt.wheely.com";
    private final String containerUsername = "?username=";
    private final String containerPassword = "&password=";
    private WebSocketConnection mConnection = new WebSocketConnection();

    public static SimpleWebSocketConnector getInstance() {
        if(singleton == null){
            synchronized (SimpleWebSocketConnector.class){
                if(singleton == null)
                    singleton = new SimpleWebSocketConnector();
            }
        }
        return singleton;
    }

    public void setAuthorisation(String username, String password){
        _username = username;
        _password = password;
    }

    public  SimpleWebSocketConnector() {

    }

    public void startWebSocket(){

        try {

            WebSocketOptions WSO = new WebSocketOptions();
            mConnection.connect(severRequestStr+containerUsername+_username+containerPassword+_password
                    , new WebSocketHandler() {
                @Override
                public void onOpen() {
                    System.out.println("--open");
                    Log.d("LOG_TAG","--open");
                    try {
                        authorisationInterface.authorisationResult(true,0);
                    }catch (Exception ex){
                        Log.e("LOG_TAG",ex.toString());
                    }
                }

                @Override
                public void onTextMessage(String message) {
                    Log.d("LOG_TAG","--received message: "+message);
                    JsonParcer(message);
                }

                @Override
                public void onClose(int code, String reason) {
                    System.out.println("--close");
                    Log.d("LOG_TAG","--close"+reason);
                    authorisationInterface.authorisationResult(false,code);
                }
            });

        } catch (Exception ex) {
            Log.e("LOG_TAG",ex.toString());
        }
    }

    public boolean isConnection(){
        return mConnection.isConnected();
    }

    public void sendByWebSocket(String jsonStr){
        mConnection.sendTextMessage(jsonStr);
    }

    //получает строку в json формате и формирует массив точек
    private void JsonParcer(String strJson){
        String id   = "id";
        String lat  = "lat";
        String lon  = "lon";
        JSONArray array;
        ArrayList<MapPosition> arrayList = new ArrayList<>();
        try {
            array = new JSONArray(strJson);
            for (int i = 0; i < array.length(); i++) {
                JSONObject mapObject = array.getJSONObject(i);
                MapPosition mapPosition = new MapPosition();
                if (mapObject.has(id))
                    mapPosition.setId(mapObject.getInt(id));
                if (mapObject.has(lat))
                    mapPosition.setLat(mapObject.getDouble(lat));
                if (mapObject.has(lon))
                    mapPosition.setLon(mapObject.getDouble(lon));
                arrayList.add(mapPosition);

            }
            mapInterface.addMapPosition(arrayList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void disconnectWebSocket(){
        try {
            mConnection.disconnect();
        }catch (Exception ex){
            Log.e("LOG_TAG",ex.toString());
        }
    }

    public void stopWebSocket(){
        mConnection.disconnect();
        singleton = null;
        unRegisterCallBackAuthorisation();
    }

    //Интерфейс для получения точки отображения
    public interface MapInterface{
        void addMapPosition(ArrayList<MapPosition> mps);
    }

    MapInterface mapInterface;

    public void registerCallBack(MapInterface mapInterface){
        this.mapInterface = mapInterface;
    }


    //Интерфейс для получения результатов авторизации
    public interface AuthorisationInterface{
        void authorisationResult(boolean successful,int code);
    }
    AuthorisationInterface authorisationInterface;
    public void registerCallBackAuthorisation(AuthorisationInterface ai){
        this.authorisationInterface = ai;
    }

    public void unRegisterCallBackAuthorisation(){
        this.authorisationInterface = null;
    }
}
