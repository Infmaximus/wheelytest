package com.fortest.infmaximus.wheelytestapp;

import android.Manifest;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.util.ArrayList;

public class WheelyService extends Service implements SimpleWebSocketConnector.MapInterface,
        SimpleWebSocketConnector.AuthorisationInterface{

    private String notifyText  = "Работает сервис обмена положениями";
    private String notifyTitle = "Wheely";
    private String LOG_TAG     = "LOG_TAG";
    private int    idNotify    = 777;
    private String STARTFOREGROUND_ACTION = "start";
    private String STOPTFOREGROUND_ACTION = "stop";
    private JsonTransmiter jsonTransmiter;
    private SimpleWebSocketConnector SWSC;
    private final String extraUsername = "username";
    private final String extraPassword = "password";
    private String username;
    private String password;

    private Context context;

    private boolean reconnectIsStarted = false;

    public WheelyService() {
        Log.i(LOG_TAG, "Service: onCreate");

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        context = this;
        Log.i(LOG_TAG, "Service: onStartCommand");

        if (intent.hasExtra(extraUsername)) {
            username = intent.getStringExtra(extraUsername);
        }
        if (intent.hasExtra(extraPassword)) {
            password =intent.getStringExtra(extraPassword);
        }
        if (intent.getAction().equals(STARTFOREGROUND_ACTION)) {
            Log.i(LOG_TAG, "Старт ");
            createConnection();
        } else if (intent.getAction().equals(STOPTFOREGROUND_ACTION)) {
            Log.i(LOG_TAG, "Стоп");
            stopForeground(true);
            SimpleWebSocketConnector.getInstance().stopWebSocket();
            stopSelf();
        }


        return START_STICKY;
    }

    //Инициализация неубиваемого сервиса
    private void createConnection(){
        Intent simpleIntent = new Intent(this, MapsActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(this,
                0, simpleIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        Notification.Builder builder = new Notification.Builder(this)
                .setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.wh)
                .setContentTitle(getString(R.string.app_title))
                .setContentText(getString(R.string.service_notification));
        Notification notification;
        if (Build.VERSION.SDK_INT < 16)
            notification = builder.getNotification();
        else
            notification = builder.build();
        startForeground(idNotify, notification);

        SimpleWebSocketConnector.getInstance().registerCallBackAuthorisation(this);
        getLocation();
    }

    //используется для повторной отправки расположения
    private class WaiterAsyncTask extends AsyncTask<String, Integer, Integer> {
        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            getLocation();
        }
        @Override
        protected Integer doInBackground(String... parameter) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    //переподключение после разрыва
    private class MyAsyncTask extends AsyncTask<String, Integer, Integer> {
        private Context context;
        public MyAsyncTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            new WaiterAsyncTask().execute();
        }

        @Override
        protected Integer doInBackground(String... parameter) {

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            SimpleWebSocketConnector SWSC = SimpleWebSocketConnector.getInstance();
            if(!SWSC.isConnection()) {
                Log.i(LOG_TAG, "try reconnect");
                SWSC.startWebSocket();
            }
            return null;
        }
    }

    private void getLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location;
        LocationListener loc_listener = new LocationListener() {

            public void onLocationChanged(Location l) {}

            public void onProviderEnabled(String p) {}

            public void onProviderDisabled(String p) {}

            public void onStatusChanged(String p, int status, Bundle extras) {}
        };
        locationManager
                .requestLocationUpdates(bestProvider, 0, 0, loc_listener);
        location = locationManager.getLastKnownLocation(bestProvider);
        try {
            jsonTransmiter = new JsonTransmiter();
            jsonTransmiter.sendJsonPosition(location.getLatitude(),location.getLongitude());
        } catch (Exception ex) {
            Log.e(LOG_TAG,ex.toString());
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void addMapPosition(ArrayList<MapPosition> mps) {
        Log.d(LOG_TAG,"get mps server");
    }


    //результаты переавторизации
    @Override
    public void authorisationResult(boolean successful, int reason) {
        if(!successful) {
            Log.i(LOG_TAG, "try reconnect outside");
            MyAsyncTask myAsyncTask = new MyAsyncTask(context);
            myAsyncTask.execute();

        }
    }


}